<?php
/*****

** Content **

* Media
* CPT
* Theme Support

*****/

/*********/
/* Media */
/*********/

add_image_size('mobile', 	575, 	575, 	true);
add_image_size('tablet', 	768, 	768, 	true); 
add_image_size('laptop', 	992,	0, 	false); 
add_image_size('desktop', 	1230, 	0, 	false); 
add_image_size('max', 		2545, 	0, 	false); 


remove_image_size('1536x1536');
remove_image_size('2048x2048');


add_filter( 'intermediate_image_sizes_advanced', 'remove_default_images' );
// Remove default image sizes here. 
function remove_default_images( $sizes ) {
 unset( $sizes['small']); // 150px
 unset( $sizes['medium']); // 300px
 unset( $sizes['large']); // 1024px
 unset( $sizes['medium_large']); // 768px
 return $sizes;
}


/*******************/
/* Remove comments */
/*******************/

// Removes from admin menu
add_action( 'admin_menu', 'remove_admin_menus' );
function remove_admin_menus() {
 	remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'admin_bar_render' );

/*****************/
/* Theme Support */
/*****************/

add_post_type_support( 'page', 'excerpt' );
add_theme_support('post-thumbnails'); 

add_action('upload_mimes', 'add_file_types_to_uploads');

function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}


/*******/
/* CPT */
/*******/

add_action( 'init', 'create_posttypes' );
function create_posttypes() {
	
	$labels = array(
	  'name'                => __('Reportage puffar', 'api'),
	  'singular_name'       => __('Reportage puff', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'supports' 			=> array( 'title', 'editor', 'thumbnail'  )
	);
	register_post_type( 'alumni', $args );


	$labels = array(
	  'name'                => __('Citat', 'api'),
	  'singular_name'       => __('Citat', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'supports' 			=> array( 'title', 'editor', 'thumbnail'  )
	);
	register_post_type( 'quotes', $args );

	$labels = array(
	  'name'                => __('YH-Utbildningar', 'api'),
	  'singular_name'       => __('YH-Utbildning', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'utbildning/yh-utbildningar'),
	  'supports' 			=> array( 'title', 'thumbnail', 'excerpt', 'revisions'  )
	);
	register_post_type( 'programme', $args );

	$labels = array(
	  'name'                => __('Kurser', 'api'),
	  'singular_name'       => __('Kurs', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'utbildning/%course_type%'),
	  'supports' 			=> array( 'title', 'thumbnail', 'excerpt', 'revisions'   )
	);
	register_post_type( 'course', $args );

	register_taxonomy(
		'course_type', 
		'course', 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Kurs typer',
    			'singular_name' => 'Kurs typ',
			)
		)
	);

	/**/

	register_taxonomy(
		'programme_course_category', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'YH områden',
    			'singular_name' => 'YH område',
			)
		)
	);

	register_taxonomy(
		'programme_course_speed', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Studiefarter',
    			'singular_name' => 'Studiefart',
			)
		)
	);

	register_taxonomy(
		'programme_course_area', 
		array('programme', 'course', 'employee'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Orter',
    			'singular_name' => 'Ort',
			),
			'show_admin_column' => true
		)
	);

	register_taxonomy(
		'programme_course_internship', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Praktiker',
    			'singular_name' => 'Praktik',
			)
		)
	);

	register_taxonomy(
		'programme_course_meets', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Träffar',
    			'singular_name' => 'Träff',
			)
		)
	);

	register_taxonomy(
		'programme_course_period', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Terminer',
    			'singular_name' => 'Termin',
			)
		)
	);


	$labels = array(
	  'name'                => __('Personal', 'api'),
	  'singular_name'       => __('Personal', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'kontakta-oss/[hash]'),
	  'supports' 			=> array( 'title', 'thumbnail'  )
	);
	register_post_type( 'employee', $args );


	register_taxonomy(
		'employee_area', 
		'employee', 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Avdelningar',
    			'singular_name' => 'Avdelning',
			),
			'show_admin_column' => true
		)
	);

	/* See programme_course_area
	register_taxonomy(
		'employee_location', 
		array('programme', 'course'), 
		array(
			'hierarchical' => true,
			'labels' => array(
				'name' => 'Praktiker',
    			'singular_name' => 'Praktik',
			)
		)
	);*/


	$labels = array(
	  'name'                => __('Vanliga frågor', 'api'),
	  'singular_name'       => __('Vanlig fråga', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'faq'),
	  'supports' 			=> array( 'title', 'thumbnail', 'excerpt'  )
	);
	register_post_type( 'faq', $args );

	/*
	See post category
	$labels = array(
	  'name'                => __('Repotage', 'api'),
	  'singular_name'       => __('Repotage', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'repotage'),
	  'supports' 			=> array( 'title', 'thumbnail', 'excerpt'  )
	);
	register_post_type( 'documentary', $args );

	$labels = array(
	  'name'                => __('Lediga jobb', 'api'),
	  'singular_name'       => __('Ledigt jobb', 'api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'ledigt-jobb'),
	  'supports' 			=> array( 'title', 'thumbnail', 'excerpt'  )
	);
	register_post_type( 'job', $args );*/

}



/**************/
/* Permalinks */
/**************/

add_filter( 'site_url', 'custom_site_url' );

function custom_site_url( $url ) {
	if( strpos($_SERVER['REQUEST_URI'], 'yoast') !== false) {

		return str_replace('wordpress.', 'www.', $url);
	}

	if(is_admin() ){
        return $url;
    }

	return  $url;
}

// define the rest_url callback 
function filter_rest_url( $url, $path, $blog_id, $scheme ) { 
    // make filter magic happen here... 
    return str_replace('www.', 'wordpress.', $url);
}; 
         
// add the filter 
add_filter( 'rest_url', 'filter_rest_url', 10, 4 ); 

//Page

function filter_page_url( $url, $post ) {

	$url = str_replace('wordpress.', 'www.', $url);
    
    return $url;
}
//add_filter( 'page_link', 'filter_page_url', 10, 2 );

//Post

function filter_post_url( $url, $post ) {

	//$url = str_replace('wordpress.', '', $url);

	$url = str_replace('.se/', '.se/artiklar/%category%/', $url);

    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'category' );
        if( $terms ){
            $url = str_replace( '%category%' , $terms[0]->slug , $url );
        }
    }
    
    return $url;
}
add_filter( 'post_link', 'filter_post_url', 10, 2 );

//CPT

function filter_custom_post_url( $url, $post ) {

	//$url = str_replace('wordpress.', 'www.', $url);

    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'course_type' );
        if( $terms ){
            $url = str_replace( '%course_type%' , $terms[0]->slug , $url );
        }
    }

    $url = str_replace('[hash]/', '#', $url);

    return $url;
}
add_filter( 'post_type_link', 'filter_custom_post_url', 10, 2 );


function remove_preview_target()
{
    // below JS code will set Preview button's **target** attribute to **_self**, it means same tab/window.
    echo "<script>
        jQuery(document).ready(function(){
        jQuery('#post-preview').attr('target', '_blank');
        })
        </script>";

}
// this action performs in admit footer
add_action('admin_footer', 'remove_preview_target');



function filebird_json_url_to_wordpress( $url ) {

    return str_replace('www.', 'wordpress.', $url);
}
add_filter( 'filebird_json_url', 'filebird_json_url_to_wordpress' );


function set_slug_on_grouped_post($post_id) {
    // If this is a revision, get real post ID
    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }
    $is_grouped_post = get_field('group_course', $post_id);
    $has_programme_course_area = wp_get_object_terms( $post_id, 'programme_course_area' );


    $wpseo_primary_term = new WPSEO_Primary_Term( 'programme_course_area', get_the_id() );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
	$term = get_term( $wpseo_primary_term );

     if ( !is_wp_error( $term ) ) {
     	$has_programme_course_area = $term;
     } else {
     	$has_programme_course_area = $has_programme_course_area[0];
     }

    if($is_grouped_post && $has_programme_course_area) {


    	$has_programme_course_area_slug = $has_programme_course_area->slug;

    	$post_slug = sanitize_title(get_the_title($post_id)) . '-' . $has_programme_course_area_slug;

    	 // unhook this function so it doesn't loop infinitely
	    remove_action( 'save_post_course', 'set_slug_on_grouped_post' );
	    remove_action( 'save_post_programme', 'set_slug_on_grouped_post' );

	    // update the post, which calls save_post again
	    wp_update_post( 
	    	array( 
	    		'ID' => $post_id, 
	    		'post_name' => $post_slug
	    	) 
	    );

	    // re-hook this function
	    add_action( 'save_post_course', 'set_slug_on_grouped_post' );
	    add_action( 'save_post_programme', 'set_slug_on_grouped_post' );
    }

   
    
    

}
add_action( 'save_post_course', 'set_slug_on_grouped_post' );
add_action( 'save_post_programme', 'set_slug_on_grouped_post' );


function wpse_edit_post_show_excerpt( $user_login, $user ) {
	$get_post_types = get_post_types();
	
	foreach ($get_post_types as $cpt) {
		$unchecked = get_user_meta( $user->ID, 'metaboxhidden_' . $cpt, true );
	    
	    if(is_array($unchecked)) {
		    $key = array_search( 'postexcerpt', $unchecked );
		    if ( FALSE !== $key ) {
		        array_splice( $unchecked, $key, 1 );
		        update_user_meta( $user->ID, 'metaboxhidden_' . $cpt, $unchecked );
		    }
	    }
	}
}
add_action( 'wp_login', 'wpse_edit_post_show_excerpt', 10, 2 );


add_filter('wp_link_query', function($results, $query) {
	foreach ($results as $key => $object) {

		$term_obj_list = get_the_terms( $object['ID'], 'programme_course_area' );

	    if($term_obj_list) {
	        $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name')); 
	        
	  	    $results[$key]['title'] = $object['title'] . ' - ' . $terms_string; 
	    }

		
	}

  	return $results;
}, 10, 2);


?>