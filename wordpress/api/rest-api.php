<?php
/*****

** Content **

* Routes
	- Get
	- Post
* Route callbacks
	- Get
	- Post
* Helper functions
* ACF functions
* Yoast functions
* WPML functions



*****/


/**************/
/*   Routes   */
/**************/

add_action( 'rest_api_init', 'extend_rest_routes' );
function extend_rest_routes() {

	/*******/
    /* Get */
    /*******/
    register_rest_route( 'core', 'page', array(
    	array(
            'callback' => 'api_get_page',
            'args' => array(
            	'slug',
            	'type',
            	'fields'
            )
    	)

    ));

    //Adds route /core/cpt/type=[post_type]
	register_rest_route( 'core', 'cpt', array(
    	array(
            'callback' => 'api_get_cpt',
            'args' => array(
               	'type',
               	'fields'
            )
    	)
	));

	//Adds route /core/taxonomy/type=[tax_slug]
    register_rest_route( 'core', 'taxonomy', array(
    	array(
            'callback' => 'api_get_taxonomy',
            'args' => array(
               	'type'
            )
    	)
    ));

	//Adds route /core/options
   register_rest_route( 'core', 'options', array(
    	array(
            'callback' => 'api_get_options'
    	)
	));

    //Adds route /core/sitemap
    register_rest_route( 'core', 'sitemap', array(
    	array(
            'callback' => 'api_get_sitemap'
    	)
	));


    /********/
    /* Post */
    /********/

    register_rest_route( 'core',
		'search',
		array(
			'methods' => 'POST',
	        'callback' => 'api_post_search'
    	)
    );

}

/*******************/
/* Route callbacks */
/*******************/


/*******/
/* Get */
/*******/

function api_get_page($data) {


	$args = array(
		'post_type'              => explode(',',$data['type']),
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'name'					 => $data['slug'],
	);

	/* Preview mode */
	if($data['preview']) {
	    $post = get_page_by_path( $data['slug'], OBJECT, explode(',',$data['type']) );
	    $post_ID = $post->ID;

	    $args = array(
	        'post_status' => 'any',
	        'post_parent' => $post_ID,
	        'post_type' => 'revision',
	        'sort_column' => 'ID',
	        'sort_order' => 'desc',
	        'posts_per_page' => 1
	    );
	}


	$fields = true;

	if($data['fields'] !== NULL) {
		$fields = $data['fields'];

		if($fields === 'false') {
			$fields = false;
		}
	}


	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $post) {
		$routes[] = add_custom_tags_to_response($post, $fields);
	}

	// Response setup
	return new WP_REST_Response( $routes, 200 );
}

function api_get_cpt($data) {

	$args = array(
		'post_type'              => $data['type'],
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'orderby'				 => 'title',
		'post_status'			 => array('publish')
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	$posts = json_decode(json_encode($query->posts), true);


	foreach ($posts as $post) {
		$objects = array(
			'post_date_gmt',
			'post_author',
			'post_parent',
			'menu_order',
			'post_status',
			'comment_status',
			'ping_status',
			'post_password',
			'to_ping',
			'pinged',
			'post_modified',
			'post_modified_gmt',
			'post_content_filtered',
			'guid',
			'post_mime_type',
			'comment_count',
			'filter'
		);

		foreach ($objects as $object) {
			unset($post[$object]);
		}


		$fields = true;

		if($data['type'] == 'programme' || $data['type'] == 'course') {
			$fields = ['yh-course', 'featured', 'featured_priority', 'group_course', 'show_in_archive', 'img_pos', 'apply'];
		}

		$routes[] = add_custom_tags_to_response($post, true);
	}

	// Response setup
	return new WP_REST_Response( $routes, 200 );
}

function api_get_taxonomy($data) {

	$tax = get_terms( array(
	    'taxonomy' => $data['type'],
	    'hide_empty' => false,
	) );

	// Response setup
	return new WP_REST_Response( $tax, 200 );
}

function api_get_options($data) {

	$options = 'options';

	$routes = get_fields($options);

	global $wp_admin_bar;

	$routes['admin_bar'] = $wp_admin_bar;

	return new WP_REST_Response( $routes, 200 );
}


function api_get_sitemap($data) {

	$args = array(
		'post_type'              => array('any'),
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'post_status' 			 => 'publish'
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $post) {
		$objects = array(
			'post_date_gmt',
			'post_author',
			'post_parent',
			'menu_order',
			'post_status',
			'comment_status',
			'ping_status',
			'post_password',
			'to_ping',
			'pinged',
			'post_modified',
			'post_modified_gmt',
			'post_content_filtered',
			'guid',
			'post_mime_type',
			'comment_count',
			'filter',
			'post_content',
			'post_date'
		);

		foreach ($objects as $object) {
			unset($post->{$object});
		}

		$post->permalink = str_replace(home_url(), '', get_permalink($post->ID));

		$post->keywords = get_field('keywords', $post->ID);

		$post_taxs = get_object_taxonomies($post->post_type);
		$temp = [];

		/*foreach ($post_taxs as $post_tax) {
			$temp[$post_tax] = get_the_terms($post->ID, $post_tax);
		}*/

		foreach ($post_taxs as $post_tax) {
			$temp[$post_tax] = get_the_terms($post->ID, $post_tax);

			if($temp[$post_tax]){
				foreach ($temp[$post_tax] as $key => $term) {
					$temp[$post_tax][$key]->primary = false;

					$wpseo_primary_term = new WPSEO_Primary_Term( $post_tax, $post->ID );
					$wpseo_primary_term = $wpseo_primary_term->get_primary_term();

					$temp[$post_tax][$key]->primary = $wpseo_primary_term === $term->term_id;
				}
			}
		}

		$post->post_taxs = $temp;

		$routes[] = $post;

	}

	return new WP_REST_Response( $routes, 200 );
}

/********/
/* Post */
/********/

function api_post_search($data) {
	$parameters = $data->get_params();

	$slug = $parameters['slug'];
	$type = $parameters['type'];
	$rows = get_field('popularsearch', 'options');
	$rowId = false;
	$rowAmount = false;


	foreach ($rows as $key => $row) {
		if($row['slug'] == $slug) {
			$rowId = $key + 1;
			$rowAmount = $row['amount'];
		}
	}

	if($rowId) {

		$row = array(
		    'slug' => $slug,
		    'type' => $type,
		    'amount' => $rowAmount + 1
		);

		update_row('popularsearch', $rowId, $row, 'options');

	}else {

		$row = array(
		    'slug' => $slug,
		    'type'   => $type,
		    'amount'  => 1
		);

		add_row('popularsearch', $row, 'options');
	}

	return new WP_REST_Response( true , 200 );
}





/********************/
/* Helper functions */
/********************/

function add_custom_tags_to_response( $post, $custom_fields = true ) {
	global $shortcode_tags;


	$post = json_decode(json_encode($post), true);

	//permalink
	$post['permalink'] = str_replace(home_url(), '', get_permalink($post['ID']));

	//content formating
	remove_filter( 'the_content', 'do_shortcode', 11 );
	$post['post_content'] = apply_filters('the_content', $post['post_content']);
	add_filter( 'the_content', 'do_shortcode', 11 );

	//trigger shortcodes
	$post_content = $post['post_content'];

	preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $post_content, $matches );
    $tagnames = array_intersect( array_keys( $shortcode_tags ), $matches[1] );
    $m = array();

    //Images
	$post['featured_image'] = get_image_src(get_post_thumbnail_id($post['ID']));

	//Tags
	$post_taxs = get_object_taxonomies($post['post_type']);

	foreach ($post_taxs as $post_tax) {
		$post['post_taxs'][$post_tax] = get_the_terms($post['ID'], $post_tax);

		if($post['post_taxs'][$post_tax]){
			foreach ($post['post_taxs'][$post_tax] as $key => $term) {
				$post['post_taxs'][$post_tax][$key]->primary = false;

				$wpseo_primary_term = new WPSEO_Primary_Term( $post_tax, $post['ID'] );
				$wpseo_primary_term = $wpseo_primary_term->get_primary_term();

				$post['post_taxs'][$post_tax][$key]->primary = $wpseo_primary_term === $term->term_id;
			}
		}
	}

	//Custom fields
	if($custom_fields) {
		$post['custom_fields'] = get_acf_fields($post['ID'], $custom_fields);
	}

	//WPML
	$post['language'] = apply_filters( 'wpml_post_language_details', NULL, $post['ID'] );

	//Remove some objects
	$objects = array(
		'post_date_gmt',
		'post_status',
		'comment_status',
		'ping_status',
		'post_password',
		'to_ping',
		'pinged',
		'post_modified',
		'post_modified_gmt',
		'post_content_filtered',
		'guid',
		'post_mime_type',
		'comment_count',
		'filter'
	);

	foreach ($objects as $object) {
		unset($post[$object]);
	}

	return $post;
}


function get_image_src( $id ) {
	// Grab all available image sizes which have been declared with 'add_image_size'
	$image_sizes = get_intermediate_image_sizes();


	unset( $image_sizes['small']); // 150px
	unset( $image_sizes['medium']); // 300px
	unset( $image_sizes['large']); // 1024px
	unset( $image_sizes['medium_large']); // 768px

	//$image_sizes[] = 'full';

	$feat_img_array = array();

	foreach($image_sizes as $image_size) {
		$image_arr = wp_get_attachment_image_src( $id, $image_size, false);

		if(is_array($image_arr)) {
			$feat_img_array[$image_size] = $image_arr[0];
		}
	}

	$image = is_array( $feat_img_array ) ? $feat_img_array : 'false';

	return $image;
}


function get_acf_fields($post_ID, $custom_fields = true) {

	$fields = false;


	if(is_array($custom_fields)) {
		$fields = array();

		foreach ($custom_fields as $custom_field) {
			$fields[$custom_field] = get_field($custom_field, $post_ID);
		}

	} elseif ($custom_fields) {
		$fields = get_fields($post_ID);
	}

	return $fields;
}

/*****************/
/* ACF functions */
/*****************/


//Add image sizes to gallery
add_filter('acf/format_value/type=gallery', 'add_images_to_gallery', 15, 3);
function add_images_to_gallery( $value, $post_id, $field ) {

	$images = array();

	foreach ($value as $key => $image) {
		$images[$key] = add_images($image);
	}

	return $images;
}


//Add image sizes to images
add_filter('acf/format_value/type=image', 'add_images', 15, 3);
function add_images( $value, $post_id = 0, $field = '' ) {


	if(is_numeric($value) && !is_object($value)) {
		$value = get_image_src($value);
	}



	if(is_array($value)) {
		$objects = array(
			'id',
			'filename',
			'filesize',
			'url',
			'link',
			'author',
			'name',
			'status',
			'date',
			'uploaded_to',
			'modified',
			'menu_order',
			'guid',
			'mime_type',
			'type',
			'subtype',
			'icon',
			'width',
			'height'
		);

		foreach ($objects as $object) {
			unset($value[$object]);
		}


		$value['custom_fields'] = get_acf_fields($value['ID']);
	}

    return $value;
}

add_filter('acf/format_value/type=link', 'remove_domain', 15, 3);
function remove_domain( $value, $post_id, $field ) {

	if($value && $value['url']) {
		$value['url'] = str_replace(home_url(), '', $value['url']);
	}



    return $value;
}

//Add ACF to relationship
add_filter('acf/format_value/type=relationship', 'add_post_data', 15, 3);
add_filter('acf/format_value/type=post_object', 'add_post_data', 15, 3);
function add_post_data( $value, $post_id, $field ) {
	$res = array();

	if(is_array($value)){
		foreach ($value as $key => $v) {

			if(is_object($v)) {

				$get_fields = false;

				if($v->post_type == 'alumni' || $v->post_type == 'quotes') {
					$get_fields = true;
				}

				if($v->post_type == 'course' || $v->post_type == 'programme') {
					$get_fields = ['img_pos', 'group_course', 'show_in_archive', 'yh-course'];
				}

				if($v->post_type == 'post' || $v->post_type == 'faq' || $v->post_type == 'page') {
					$get_fields = ['img_pos'];
				}

				$res[] = add_custom_tags_to_response($v, $get_fields);
			} else {
				$res[] = array(
					'ID' => $v,
					'post_name' => get_post_field('post_name' , $v),
					'permalink' => str_replace(home_url(), '', get_permalink($v))
				);

			}
		}
	} elseif (is_object($value)) {


		$get_fields = false;

		if($value->post_type == 'alumni' || $value->post_type == 'employee') {
			$get_fields = true;
		}

		$res = add_custom_tags_to_response($value, $get_fields);

	} else {
		$res = $value;
	}


    return $res;
}

?>