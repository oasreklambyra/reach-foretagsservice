exports.ids = [12];
exports.modules = {

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheCookies.vue?vue&type=template&id=088d33a4&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (!_vm.isChecked)?_c('div',{staticClass:"cookies-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"row\">","</div>",[_vm._ssrNode("<div class=\"col-6 text-wrapper\"><h3>Cookies</h3> <h5>\n          Vi använder cookies för att ge dig en bättre upplevelse på vår\n          webbplats.\n        </h5></div> "),_vm._ssrNode("<div class=\"col-6 buttons-wrapper\">","</div>",[_c('b-button',{attrs:{"variant":"secondary","to":"/Integritetspolicy"}},[_c('span',[_vm._v("Läs mer om cookies här")]),_vm._v(" "),_c('svg-icon',{attrs:{"name":"reachhemservice/chevron"}})],1),_vm._ssrNode(" "),_c('b-button',{attrs:{"variant":"primary"},on:{"click":_vm.checkCookies}},[_c('span',[_vm._v("Jag godkänner alla cookies")]),_vm._v(" "),_c('svg-icon',{attrs:{"name":"reachhemservice/accept"}})],1)],2)],2)]),_vm._ssrNode(" <div></div>")],2):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheCookies.vue?vue&type=template&id=088d33a4&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheCookies.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheCookiesvue_type_script_lang_js_ = ({
  components: {},

  data() {
    return {
      isChecked: true
    };
  },

  mounted() {
    this.isChecked = window.localStorage.isCookiesChecked;

    if (this.isChecked === 'true') {
      this.$gtm.init();
    }
  },

  methods: {
    checkCookies() {
      window.localStorage.isCookiesChecked = true;
      this.isChecked = true;
      this.$gtm.init();
    }

  }
});
// CONCATENATED MODULE: ./components/TheCookies.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheCookiesvue_type_script_lang_js_ = (TheCookiesvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/TheCookies.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(75)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheCookiesvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "9575b724"
  
)

/* harmony default export */ var TheCookies = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 51:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("13588d1e", content, true, context)
};

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheCookies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(51);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheCookies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheCookies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheCookies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheCookies_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".cookies-wrapper{position:fixed;bottom:2.5rem;left:0;width:100%;display:flex;justify-content:center;align-items:center;z-index:999}.cookies-wrapper .container{width:1920px;max-width:100%;background:#ffda5c;padding:2rem;border-radius:.8rem}@media (max-width:1920px){.cookies-wrapper .container{width:95%}}.cookies-wrapper .container .row{justify-content:space-between;align-items:center}@media (max-width:1668px){.cookies-wrapper .container .row{justify-content:center}}.cookies-wrapper .container .row .col-6{padding:0}@media (max-width:1668px){.cookies-wrapper .container .row .col-6{flex:0 0 100%;max-width:100%}}.cookies-wrapper .container .row .col-6 h3{margin-bottom:1rem}.cookies-wrapper .container .row .col-6.text-wrapper h5{font-family:\"DM Sans\",sans-serif;margin:0}@media (max-width:1668px){.cookies-wrapper .container .row .col-6.text-wrapper{margin-bottom:1rem;text-align:center}}.cookies-wrapper .container .row .col-6.buttons-wrapper{justify-content:flex-end}@media (max-width:1668px){.cookies-wrapper .container .row .col-6.buttons-wrapper{justify-content:center}.cookies-wrapper .container .row .col-6.buttons-wrapper .btn{margin:0 1rem;text-align:left}}@media (max-width:1668px) and (max-width:767px){.cookies-wrapper .container .row .col-6.buttons-wrapper .btn{margin-top:1rem}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=12.js.map