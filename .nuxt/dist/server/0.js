exports.ids = [0];
exports.modules = {

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/ThePagebuilder.vue?vue&type=template&id=165c9394&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pagebuilder-wrapper"},_vm._l((_vm.blocks),function(block,index){return _vm._ssrNode("<div"+(_vm._ssrAttr("id",block.acf_fc_layout + '-' + index))+(_vm._ssrClass(null,['block-wrapper', block.acf_fc_layout]))+">","</div>",[(block.acf_fc_layout === 'base_content')?_c('PagebuilderBaseContent',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_cta')?_c('PagebuilderBaseCta',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_employee')?_c('PagebuilderBaseEmployee',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_service')?_c('PagebuilderBaseService',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_news')?_c('PagebuilderBaseNews',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_price_calculator')?_c('PagebuilderBasePriceCalculator',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_contact')?_c('PagebuilderBaseContact',{attrs:{"data":block}}):_vm._e(),_vm._ssrNode(" "),(block.acf_fc_layout === 'base_testimonials')?_c('PagebuilderBaseTestimonials',{attrs:{"data":block}}):_vm._e()],2)}),0)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/ThePagebuilder.vue?vue&type=template&id=165c9394&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/ThePagebuilder.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ThePagebuildervue_type_script_lang_js_ = ({
  components: {
    PagebuilderBaseContent: () => __webpack_require__.e(/* import() */ 17).then(__webpack_require__.bind(null, 108)),
    PagebuilderBaseCta: () => __webpack_require__.e(/* import() */ 18).then(__webpack_require__.bind(null, 109)),
    PagebuilderBaseEmployee: () => __webpack_require__.e(/* import() */ 19).then(__webpack_require__.bind(null, 110)),
    PagebuilderBaseService: () => __webpack_require__.e(/* import() */ 22).then(__webpack_require__.bind(null, 111)),
    PagebuilderBaseNews: () => __webpack_require__.e(/* import() */ 20).then(__webpack_require__.bind(null, 112)),
    PagebuilderBasePriceCalculator: () => __webpack_require__.e(/* import() */ 21).then(__webpack_require__.bind(null, 113)),
    PagebuilderBaseContact: () => __webpack_require__.e(/* import() */ 16).then(__webpack_require__.bind(null, 114)),
    PagebuilderBaseTestimonials: () => __webpack_require__.e(/* import() */ 23).then(__webpack_require__.bind(null, 115))
  },
  props: {
    blocks: {
      type: [Array, Boolean]
    },
    content: {
      typeblocks: Object,
      default: () => {
        return {};
      }
    }
  }
});
// CONCATENATED MODULE: ./components/ThePagebuilder.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_ThePagebuildervue_type_script_lang_js_ = (ThePagebuildervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/ThePagebuilder.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(77)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_ThePagebuildervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "453523ea"
  
)

/* harmony default export */ var ThePagebuilder = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("37782eeb", content, true, context)
};

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(52);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_ThePagebuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".pagebuilder-wrapper{margin:10vmin 0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=0.js.map