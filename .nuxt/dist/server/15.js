exports.ids = [15];
exports.modules = {

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheMenu.vue?vue&type=template&id=5cce6c02&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:[_vm.isActive ? 'active' : 'noActive', 'box'],attrs:{"id":"menu-wrapper"}},[_c('nuxt-link',{staticClass:"logo",attrs:{"to":"/"}},[_c('svg-icon',{attrs:{"name":"reachhemservice/reachforetagsservice-logo"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"navigation\">","</div>",[_vm._ssrNode("<nav>","</nav>",[_vm._ssrNode("<ul class=\"main-nav\">","</ul>",_vm._l((_vm.menu),function(link_item,i){return _vm._ssrNode("<li>","</li>",[_c('nuxt-link',{attrs:{"to":link_item.link.url}},[_vm._v("\n            "+_vm._s(link_item.link.title)+"\n          ")])],1)}),0),_vm._ssrNode(" <div id=\"burger-wrapper\""+(_vm._ssrClass(null,[_vm.isActive ? 'active' : 'noActive']))+"><span class=\"line top\"></span> <span class=\"line middle\"></span> <span class=\"line bottom\"></span></div> "),_vm._ssrNode("<div"+(_vm._ssrClass(null,[_vm.isActiveBooking ? 'active buttons-wrapper cta-button-wrapper' : 'noActive buttons-wrapper cta-button-wrapper']))+">","</div>",[_c('b-button',{staticClass:"orange",attrs:{"variant":"primary"}},[_c('svg-icon',{attrs:{"name":"reachhemservice/ok.svg"}}),_vm._v(" "),_c('span',{class:[_vm.isActiveBooking ? 'active' : 'noActive']},[_c('span',{staticClass:"swapText"},[_vm._v("Boka ett besök")])])],1)],1)],2)]),_vm._ssrNode(" "),_vm._ssrNode("<div"+(_vm._ssrClass(null,[_vm.isActiveBooking ? 'active booking-wrapper' : 'booking-wrapper', 'border']))+">","</div>",[_vm._ssrNode("<div class=\"booking-wrapper-inner\">","</div>",[_vm._ssrNode("<div class=\"close-dialog\">","</div>",[_c('svg-icon',{attrs:{"name":"reachhemservice/cancel"}}),_vm._ssrNode(" <span>Stäng</span>")],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"inner-content\">","</div>",[_c('BaseContactform',{attrs:{"id":"21"}})],1)],2)])],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheMenu.vue?vue&type=template&id=5cce6c02&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheMenu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheMenuvue_type_script_lang_js_ = ({
  components: {
    BaseContactform: () => __webpack_require__.e(/* import() */ 2).then(__webpack_require__.bind(null, 116))
  },

  data() {
    return {
      isActive: false,
      isActiveBooking: false
    };
  },

  computed: {
    menu() {
      return this.options.links || [];
    },

    options() {
      return this.$store.state.options || {};
    }

  },
  watch: {
    $route() {
      this.isActive = false;
      this.isActiveBooking = false;
    }

  },
  methods: {
    toggleClassBooking(event) {
      if (this.isActiveBooking) {
        this.isActiveBooking = false;
      } else {
        this.isActiveBooking = true;
      }
    },

    removeClassBooking(event) {
      this.isActiveBooking = true;
    },

    toggleClass(event) {
      if (this.isActive) {
        this.isActive = false;
      } else {
        this.isActive = true;
      }
    },

    removeClass(event) {
      this.isActive = true;
    }

  }
});
// CONCATENATED MODULE: ./components/TheMenu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheMenuvue_type_script_lang_js_ = (TheMenuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/TheMenu.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(79)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheMenuvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "251081e0"
  
)

/* harmony default export */ var TheMenu = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(80);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("69e61ad5", content, true, context)
};

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(53);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "#menu-wrapper{display:flex;justify-content:center;align-items:flex-start;flex-wrap:wrap;width:100%;height:88px;background:#fff;padding:0;transition:all .25s ease;position:relative}#menu-wrapper.active{height:calc(100vh - 4rem)}#menu-wrapper.active .navigation{overflow:visible;overflow:initial}@media (max-width:1440px){#menu-wrapper{height:66px}}#menu-wrapper .logo{display:flex;align-items:center;position:absolute;top:-30px;left:-.2rem;z-index:999}#menu-wrapper .logo svg{width:320px;height:150px}@media (max-width:1440px){#menu-wrapper .logo svg{width:220px;height:125px}}#menu-wrapper .navigation{width:calc(100% - 3rem);height:88px;display:flex;justify-content:space-between;align-items:center;position:relative;overflow:hidden}@media (max-width:1440px){#menu-wrapper .navigation{height:66px}}#menu-wrapper .navigation nav{display:flex;justify-content:flex-end;align-items:center;width:100%;position:relative}#menu-wrapper .navigation nav #burger-wrapper{width:30px;height:16px;display:none;position:relative;margin-right:2rem;cursor:pointer}@media (max-width:600px){#menu-wrapper .navigation nav #burger-wrapper{margin-right:0}}#menu-wrapper .navigation nav #burger-wrapper:after,#menu-wrapper .navigation nav #burger-wrapper:before{content:\"\";position:absolute;width:100%;height:2px;background:#101820;left:0;top:50%;margin-top:-1px;transform:rotate(0deg);transition:all .25s ease}#menu-wrapper .navigation nav #burger-wrapper.active:before{transform:rotate(-45deg)}#menu-wrapper .navigation nav #burger-wrapper.active:after{transform:rotate(45deg)}#menu-wrapper .navigation nav #burger-wrapper.active .line{opacity:0}#menu-wrapper .navigation nav #burger-wrapper.active .line.top{transform:translateX(100%)}#menu-wrapper .navigation nav #burger-wrapper.active .line.middle{transform:translateY(-100%)}#menu-wrapper .navigation nav #burger-wrapper.active .line.bottom{transform:translateX(-100%)}@media (max-width:1280px){#menu-wrapper .navigation nav #burger-wrapper{display:flex}}#menu-wrapper .navigation nav #burger-wrapper .line{width:100%;height:2px;background:#101820;position:absolute;left:0;transition:all .25s ease}#menu-wrapper .navigation nav #burger-wrapper .line.top{top:0}#menu-wrapper .navigation nav #burger-wrapper .line.middle{top:50%;margin-top:-1px}#menu-wrapper .navigation nav #burger-wrapper .line.bottom{bottom:0}#menu-wrapper .navigation nav .main-nav{display:flex;justify-content:flex-start;align-items:center;flex-wrap:wrap;margin-right:2rem}@media (max-width:1280px){#menu-wrapper .navigation nav .main-nav{justify-content:center;position:absolute;top:0;left:0;width:100%;height:auto;transform:translateY(88px)}#menu-wrapper .navigation nav .main-nav li a{font-size:24px}}#menu-wrapper .navigation nav .main-nav li{list-style:none;margin-left:1.5rem}@media (max-width:1280px){#menu-wrapper .navigation nav .main-nav li{text-align:center;margin:1rem 0;width:100%}}#menu-wrapper .navigation nav .main-nav li a{font-family:\"DM Sans\",sans-serif;font-size:18px;font-weight:700;color:#101820;text-decoration:none;transition:all .25s ease}#menu-wrapper .navigation nav .main-nav li a.nuxt-link-exact-active,#menu-wrapper .navigation nav .main-nav li a:hover{color:#3ea938}@media (max-width:600px){#menu-wrapper .navigation nav .cta-button-wrapper{position:fixed;bottom:2.5rem;left:2.5%;width:95%}}#menu-wrapper .navigation nav .cta-button-wrapper button{margin:0;padding:.8rem;background:#ffda5c}@media (max-width:1440px){#menu-wrapper .navigation nav .cta-button-wrapper button{padding:.8rem 1rem}}@media (max-width:600px){#menu-wrapper .navigation nav .cta-button-wrapper button{padding:1.2rem}}#menu-wrapper .navigation nav .cta-button-wrapper button span{color:#101820}#menu-wrapper .navigation nav .cta-button-wrapper button svg{width:25px;height:25px;margin-right:1rem;margin-left:0;fill:#101820}#menu-wrapper .booking-wrapper{position:fixed;top:1rem;right:0;width:50%;height:calc(100vh - 2rem);overflow:hidden;display:flex;align-items:flex-start;justify-content:center;transition:all .4s ease;border-radius:.8rem;transform:translateX(100%)}#menu-wrapper .booking-wrapper.active{transform:translateX(-2rem)}@media (max-width:960px){#menu-wrapper .booking-wrapper.active{height:calc(100% - 10rem);width:calc(100% - 4rem);top:130px}}@media (max-width:600px){#menu-wrapper .booking-wrapper{width:100%}}#menu-wrapper .booking-wrapper .booking-wrapper-inner{width:100%;height:100%;padding:5rem;position:relative;overflow:hidden;background:#fff;box-shadow:-10px 0 20px rgba(0,0,0,.1);overflow-y:scroll;-webkit-overflow-scrolling:touch}@media (max-width:1440px){#menu-wrapper .booking-wrapper .booking-wrapper-inner{padding:5rem 2.5rem 0}}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog{position:absolute;right:1rem;top:1rem;width:auto;height:auto;cursor:pointer;border:2px solid #101820;border-radius:.8rem;display:flex;justify-content:space-between;align-items:center;padding:.5rem 1rem;transition:all .25s ease;background:#fff}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog:hover{background:#101820}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog:hover svg{fill:#fff}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog:hover span{color:#fff}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog svg{width:20px;height:20px;margin-right:1rem;fill:#101820}@media (max-width:600px){#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog svg{width:10px;height:10px}}#menu-wrapper .booking-wrapper .booking-wrapper-inner .close-dialog span{font-family:\"DM Sans\",sans-serif;color:#101820;font-weight:700}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=15.js.map