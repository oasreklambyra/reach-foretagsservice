exports.ids = [13];
exports.modules = {

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFooter.vue?vue&type=template&id=66dcd5e8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"footer-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"column logo-wrapper\">","</div>",[_c('nuxt-link',{attrs:{"to":"/"}},[_c('svg-icon',{attrs:{"name":"reachhemservice/reachfast-logo"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<a target=\"_blank\" href=\"https://www.serviceforetagen.se/auktorisation/\">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/asf"}})],1),_vm._ssrNode(" <div target=\"_blank\" href=\"https://www.serviceforetagen.se/auktorisation/\"><img src=\"natverk-mo.png\" alt=\"nätverk mot bla bla\"></div>")],2),_vm._ssrNode(" <div class=\"column contact-info-wrapper\"><div class=\"content\">"+(_vm._s(_vm.options.footer.footer_info))+"</div></div> "),_vm._ssrNode("<div class=\"column social-icons-wrapper\">","</div>",[_vm._ssrNode("<a"+(_vm._ssrAttr("href",_vm.options.footer.socialmedia.instagram))+">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/insta"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<a"+(_vm._ssrAttr("href",_vm.options.footer.socialmedia.facebook))+">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/fb"}})],1)],2)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/TheFooter.vue?vue&type=template&id=66dcd5e8&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/TheFooter.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var TheFootervue_type_script_lang_js_ = ({
  computed: {
    options() {
      return this.$store.state.options || {};
    }

  }
});
// CONCATENATED MODULE: ./components/TheFooter.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_TheFootervue_type_script_lang_js_ = (TheFootervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/TheFooter.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(73)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_TheFootervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1bab9f0c"
  
)

/* harmony default export */ var TheFooter = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(74);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("26342bd5", content, true, context)
};

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(50);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".footer-wrapper{padding:5rem 0 15rem}.footer-wrapper .container{display:flex;justify-content:space-between;align-items:flex-start;flex-wrap:wrap}.footer-wrapper .container .column{width:33.33%}@media (max-width:1280px){.footer-wrapper .container .column{width:100%}}.footer-wrapper .container .logo-wrapper{display:flex;justify-content:flex-start;align-items:center;flex-wrap:wrap;width:33.33%}@media (max-width:1280px){.footer-wrapper .container .logo-wrapper{display:flex;justify-content:center;flex-wrap:wrap;width:100%;margin-bottom:2rem}}@media (max-width:850px){.footer-wrapper .container .logo-wrapper{width:100%}}.footer-wrapper .container .logo-wrapper a,.footer-wrapper .container .logo-wrapper div{width:100%;margin-bottom:2rem}.footer-wrapper .container .logo-wrapper a:last-of-type,.footer-wrapper .container .logo-wrapper div:last-of-type{margin-right:0}.footer-wrapper .container .logo-wrapper a svg,.footer-wrapper .container .logo-wrapper div svg{width:50%;height:80px;margin-right:2rem}@media (max-width:1280px){.footer-wrapper .container .logo-wrapper a svg,.footer-wrapper .container .logo-wrapper div svg{margin-right:0;width:80%;margin-left:10%}}.footer-wrapper .container .logo-wrapper a img,.footer-wrapper .container .logo-wrapper div img{width:50%;height:auto;margin-right:2rem}@media (max-width:1200px){.footer-wrapper .container .logo-wrapper a img,.footer-wrapper .container .logo-wrapper div img{margin-right:0;width:30%;margin-left:35%}}@media (max-width:768px){.footer-wrapper .container .logo-wrapper a img,.footer-wrapper .container .logo-wrapper div img{margin-right:0;width:50%;margin-left:25%}}@media (max-width:400px){.footer-wrapper .container .logo-wrapper a img,.footer-wrapper .container .logo-wrapper div img{margin-right:0;width:80%;margin-left:10%}}.footer-wrapper .container .contact-info-wrapper .content{text-align:center;margin:0}.footer-wrapper .container .contact-info-wrapper .content p{font-size:18px;margin-bottom:0;font-family:\"DM Sans\",sans-serif}.footer-wrapper .container .social-icons-wrapper{display:flex;justify-content:flex-end;align-items:center}@media (max-width:1024px){.footer-wrapper .container .social-icons-wrapper{justify-content:center;margin-top:2rem}}.footer-wrapper .container .social-icons-wrapper a{margin-left:2rem}@media (max-width:1024px){.footer-wrapper .container .social-icons-wrapper a{margin-left:1rem;margin-right:1rem}}.footer-wrapper .container .social-icons-wrapper a svg{width:30px;height:30px}@media (max-width:600px){.footer-wrapper .container .social-icons-wrapper a svg{width:25px;height:25px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=13.js.map