exports.ids = [19];
exports.modules = {

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseEmployee.vue?vue&type=template&id=885be0ca&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"base_employee-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_c('BaseBlockTop',{attrs:{"title":_vm.data.title,"url":_vm.data.link.url,"linkTitle":_vm.data.link.title}}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row gutter\">","</div>",_vm._l((_vm.data.employee),function(employee,i){return _vm._ssrNode("<div class=\"col-md-4 col-sm-6 employee-item\">","</div>",[_vm._ssrNode("<div class=\"employee-item-inner box\">","</div>",[_vm._ssrNode("<img"+(_vm._ssrAttr("src",employee.featured_image.medium))+(_vm._ssrAttr("alt",employee.post_name))+"> "),_vm._ssrNode("<div class=\"meta\">","</div>",[_vm._ssrNode("<h5 class=\"title\">"+_vm._ssrEscape("\n              "+_vm._s(employee.custom_fields.title)+"\n            ")+"</h5> <h4>"+_vm._ssrEscape(_vm._s(employee.post_title))+"</h4> <h5>"+_vm._ssrEscape(_vm._s(employee.custom_fields.phonenumber))+"</h5> "+((employee.custom_fields.area_expertise)?("<h5>\n              Ansvarig för: <span>"+_vm._ssrEscape(_vm._s(employee.custom_fields.area_expertise))+"</span></h5>"):"<!---->")+" "),_vm._ssrNode("<a"+(_vm._ssrAttr("href",'mailto:' + employee.custom_fields.email))+">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/mail"}}),_vm._ssrNode(" <span>"+_vm._ssrEscape(_vm._s(employee.custom_fields.email.slice(0,10) + '...'))+"</span>")],2)],2)],2)])}),0)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseEmployee.vue?vue&type=template&id=885be0ca&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseEmployee.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderBaseEmployeevue_type_script_lang_js_ = ({
  components: {
    BaseBlockTop: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 117))
  },
  props: {
    data: {
      type: [Object, Array]
    }
  }
});
// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseEmployee.vue?vue&type=script&lang=js&
 /* harmony default export */ var blocks_PagebuilderBaseEmployeevue_type_script_lang_js_ = (PagebuilderBaseEmployeevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseEmployee.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(85)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  blocks_PagebuilderBaseEmployeevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "63c030b2"
  
)

/* harmony default export */ var PagebuilderBaseEmployee = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(86);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("421e86cc", content, true, context)
};

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(56);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseEmployee_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".base_employee-wrapper .container .row.gutter{margin:0 -2rem}.base_employee-wrapper .container .row.gutter>*{padding:0 2rem}.base_employee-wrapper .container .row .employee-item{margin-bottom:3rem}.base_employee-wrapper .container .row .employee-item .employee-item-inner{display:flex;justify-content:center;align-items:center;flex-wrap:wrap;height:auto}@media (max-width:1024px){.base_employee-wrapper .container .row .employee-item .employee-item-inner{justify-content:center}}.base_employee-wrapper .container .row .employee-item .employee-item-inner img{width:30%;height:auto;-o-object-fit:cover;object-fit:cover;border-radius:.8rem}@media (max-width:1024px){.base_employee-wrapper .container .row .employee-item .employee-item-inner img{width:60%;margin-bottom:1rem}}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta{width:60%;padding-left:1rem}@media (max-width:1024px){.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta{width:100%;padding:0}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta *{text-align:center}}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta *{font-family:\"DM Sans\",sans-serif;font-weight:300}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta .title{font-size:12px;letter-spacing:1.5px;text-transform:uppercase}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a,.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h4,.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h5{margin:0;font-size:16px;color:#101820}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a span,.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h4 span,.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h5 span{font-weight:700}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a{display:flex;align-items:center;transition:all .25s ease}@media (max-width:1024px){.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a{justify-content:center}}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a svg{width:20px;height:20px;fill:#101820;margin-right:.5rem;transition:all .25s ease}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a:hover{color:#3ea938}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta a:hover svg{fill:#3ea938}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h5{margin-bottom:.2rem}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta h4{font-size:18px;margin-bottom:.2rem;font-weight:700}.base_employee-wrapper .container .row .employee-item .employee-item-inner .meta .buttons-wrapper .btn{padding:.2rem .5rem}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=19.js.map