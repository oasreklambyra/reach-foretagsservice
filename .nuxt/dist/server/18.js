exports.ids = [18];
exports.modules = {

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseCta.vue?vue&type=template&id=19f86572&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"base_cta-wrapper"},[_vm._ssrNode("<div"+(_vm._ssrClass(null,['container', _vm.data.background]))+">","</div>",[_vm._ssrNode("<div"+(_vm._ssrClass(null,[ _vm.data.layout, 'row', 'gutter', 'justify-content-center', 'align-items-center']))+">","</div>",[_vm._ssrNode("<div class=\"column col-md-7\">","</div>",[_vm._ssrNode("<h2>"+_vm._ssrEscape(_vm._s(_vm.data.title))+"</h2> <div class=\"content\">"+(_vm._s(_vm.data.content))+"</div> "),_vm._ssrNode("<div class=\"buttons-wrapper\">","</div>",_vm._l((_vm.data.buttons),function(button,i){return _c('nuxt-link',{key:i,class:['btn btn-secondary', button.button_color],attrs:{"target":button.link.target,"to":button.link.url}},[_c('span',[_vm._v(_vm._s(button.link.title))]),_vm._v(" "),_c('svg',{attrs:{"width":"11","height":"16","viewBox":"0 0 11 16","fill":"none","xmlns":"http://www.w3.org/2000/svg"}},[_c('path',{attrs:{"d":"M0 15.5225L5.96923 8.00298L0 0.522461H4.28718L10.2359 8.00298L4.26667 15.5225H0Z","fill":"#1D1D1B"}})])])}),1)],2),_vm._ssrNode(" <div class=\"column col-md-5\"><img"+(_vm._ssrAttr("src",_vm.data.illustrationimage.sizes.large))+" alt=\"data.illustrationimage.alt\""+(_vm._ssrClass(null,{ 'no-shadow' : _vm.data.background === 'bg' }))+"></div>")],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseCta.vue?vue&type=template&id=19f86572&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseCta.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderBaseCtavue_type_script_lang_js_ = ({
  props: {
    data: {
      type: Object
    }
  }
});
// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseCta.vue?vue&type=script&lang=js&
 /* harmony default export */ var blocks_PagebuilderBaseCtavue_type_script_lang_js_ = (PagebuilderBaseCtavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseCta.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(83)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  blocks_PagebuilderBaseCtavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "3be68a88"
  
)

/* harmony default export */ var PagebuilderBaseCta = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 55:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("182dbcb6", content, true, context)
};

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(55);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseCta_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".base_cta-wrapper .container{padding:5rem}.base_cta-wrapper .container.bg{background:#fff;box-shadow:10px 10px 30px rgba(16,24,32,.1);border:1px solid transparent;border-radius:.8rem}@media (max-width:1024px){.base_cta-wrapper .container{padding:2.5rem}}.base_cta-wrapper .container .row.gutter{margin:0 -2rem}.base_cta-wrapper .container .row.gutter>*{padding:0 2rem}@media (max-width:767px){.base_cta-wrapper .container .row.gutter{margin:0}.base_cta-wrapper .container .row.gutter>*{padding:0}}.base_cta-wrapper .container .row .column img{width:100%;height:auto;border-radius:.8rem;box-shadow:10px 10px 30px rgba(16,24,32,.1);border:1px solid transparent}.base_cta-wrapper .container .row .column img.no-shadow{box-shadow:none;border:none}.base_cta-wrapper .container .row .column .buttons-wrapper a{margin-bottom:2rem}.base_cta-wrapper .container .row .column .buttons-wrapper a.yello{background:#ffda5c;border:none}.base_cta-wrapper .container .row .column .buttons-wrapper a.green{background:#3ea938;border:none}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=18.js.map