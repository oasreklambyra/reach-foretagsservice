exports.ids = [2];
exports.modules = {

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContactform.vue?vue&type=template&id=413b2482&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"cf-wrapper"},[_c('client-only',[_c('div',{staticClass:"form-wrapper",domProps:{"innerHTML":_vm._s(_vm.formHtml)}})])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseContactform.vue?vue&type=template&id=413b2482&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseContactform.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
/* harmony default export */ var BaseContactformvue_type_script_lang_js_ = ({
  /****************************************************************************************/

  /*                                                                                      */

  /* Add following to middleware/getSupportData.js                                        */

  /* await context.app.store.dispatch({ type: 'getContactform', id: [ID], lang: [LANG] }) */

  /*                                                                                      */

  /****************************************************************************************/
  props: {
    id: {
      type: [Number, String]
    }
  },
  computed: {
    form() {
      return this.$store.state.cf7.cf[this.id] || {};
    },

    formHtml() {
      return this.form.html;
    },

    formScripts() {
      return this.form.js;
    },

    formStyle() {
      return this.form.css;
    }

  },

  mounted() {
    // Needs to load after form html
    if (this.formScripts && !this.scriptsLoaded) {
      setTimeout(() => {
        const cfController = document.createElement('script');
        cfController.setAttribute('src', this.formScripts.scripts, 'id');
        document.body.appendChild(cfController);
        this.scriptsLoaded = true;
      }, 500);
    }
  },

  head() {
    return {
      link: [{
        rel: 'stylesheet',
        type: 'text/css',
        href: this.formStyle.main
      }],
      script: [{
        hid: 'jquery',
        async: true,
        type: 'text/javascript',
        src: this.formScripts.jquery
      }, {
        hid: 'jquery-migrate',
        async: true,
        type: 'text/javascript',
        src: this.formScripts['jquery-migrate']
      }, {
        hid: 'wpcf7',
        type: 'text/javascript',
        innerHTML: this.formScripts.wpcf7
      }],
      __dangerouslyDisableSanitizersByTagID: {
        'wpcf7': ['innerHTML']
      }
    };
  }

});
// CONCATENATED MODULE: ./components/BaseContactform.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseContactformvue_type_script_lang_js_ = (BaseContactformvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/BaseContactform.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(97)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseContactformvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "faf22d1a"
  
)

/* harmony default export */ var BaseContactform = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(98);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("405dd916", content, true, context)
};

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(62);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseContactform_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".cf-wrapper form{display:flex;flex-wrap:wrap;margin-bottom:5rem}.cf-wrapper form p{width:100%;display:flex;flex-wrap:wrap}.cf-wrapper form p label{width:100%;font-style:italic;font-family:\"DM Sans\",sans-serif}.cf-wrapper form p label input,.cf-wrapper form p label textarea{width:100%;background:hsla(0,0%,100%,.4);border:1px solid rgba(0,0,0,.1);border-radius:.8rem;padding:.75rem}.cf-wrapper form p .wpcf7-acceptance .wpcf7-list-item{margin:0;display:flex}.cf-wrapper form p .wpcf7-acceptance .wpcf7-list-item label{display:flex;justify-content:flex-start;align-items:flex-start}.cf-wrapper form p .wpcf7-acceptance .wpcf7-list-item label input{width:auto}.cf-wrapper input:-internal-autofill-selected{background-color:rgba(62,169,56,.1)!important}.cf-wrapper .wpcf7 form .wpcf7-response-output{width:100%;margin:2rem 0 0;padding:1rem;border:1px solid transparent;border-radius:.8rem;background:rgba(62,169,56,.1);font-family:\"DM Sans\",sans-serif!important}.cf-wrapper input[type=submit]{border:none;outline:none;background-color:none;background:url(/chevron.svg) no-repeat 88%;background-size:10px;transition:all .25s ease}.cf-wrapper input[type=submit] *{color:#fff}.cf-wrapper input[type=submit]:hover{background:none;background:url(/chevron.svg) no-repeat 90%!important;background-size:10px!important}.cf-wrapper input[type=checkbox]{display:none}.cf-wrapper input[type=checkbox] *{transition:all .25s ease}.cf-wrapper input[type=checkbox]+.wpcf7-list-item-label:before{content:\"\";margin-right:10px;display:inline-block;width:20px;height:20px;background:url(/checkbox-unchecked-1.svg) no-repeat 50%;background-size:contain;transform:translateX(0) translateY(5px);border:2px solid #101820;background-color:rgba(16,24,32,0)}.cf-wrapper input[type=checkbox]:checked+.wpcf7-list-item-label:before{content:\"\";display:inline-block;width:20px;height:20px;background:url(/checked.svg) no-repeat 50%;background-size:12px;transform:translateX(0) translateY(5px);border:2px solid #101820;background-color:rgba(0,128,0,.1)}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=2.js.map