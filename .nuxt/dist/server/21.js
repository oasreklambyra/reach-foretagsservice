exports.ids = [21];
exports.modules = {

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBasePriceCalculator.vue?vue&type=template&id=1711a9bc&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"base_price-calculator-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"row gutter align-items-center\">","</div>",[_vm._ssrNode("<div class=\"col-md-6\"><h2>"+_vm._ssrEscape(_vm._s(_vm.data.title))+"</h2> <div>"+(_vm._s(_vm.data.text))+"</div></div> "),_vm._ssrNode("<div class=\"col-md-6\">","</div>",[_vm._ssrNode("<div class=\"form-inner box\">","</div>",[_vm._ssrNode("<h2>Priskalkylator</h2> <p>Här kan vi lägga en generell text kring vad detta är. Typ att det är en priskalkylator och att den baseras på att man bor inom Jönköpings kommun område och vad det innebär.</p> <form action><div class=\"input-item\"><label for=\"houseType\">Välj hustyp</label> <select id name><option value>\n                  Villa\n                </option> <option value>\n                  Radhus\n                </option> <option value>\n                  Lägenhet\n                </option></select></div> <div class=\"input-item\"><label for=\"houseType\">Antal kvm</label> <select id name><option value>\n                  91-100\n                </option> <option value>\n                  101-120\n                </option> <option value>\n                  121-140\n                </option></select></div></form> <div class=\"payment\"><h4>Ungefärligt pris efter <a target=\"_blank\" href=\"https://www.skatteverket.se/privat/fastigheterochbostad/rotochrutarbete.4.2e56d4ba1202f95012080002966.html\">RUT</a></h4> <h4>"+_vm._ssrEscape(_vm._s(_vm.price * 25)+" kr")+"</h4></div> "+((_vm.area === 'jönköping' || _vm.area === 'Jönköping' || _vm.area === 'JÖNKÖPING')?("<div></div>"):(_vm.area === '')?("<div></div>"):(_vm.area != 'jönköping' || _vm.area != 'Jönköping' || _vm.area != 'JÖNKÖPING')?("<div class=\"message\">"+_vm._ssrEscape("\n            "+_vm._s(_vm.data.message)+"\n          ")+"</div>"):"<!---->")+" "),_vm._ssrNode("<div class=\"buttons-wrapper lg\">","</div>",[_vm._ssrNode("<a href=\"/\" class=\"btn btn-primary orange\">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/phone"}}),_vm._ssrNode(" <span>Ring oss</span>")],2),_vm._ssrNode(" "),_vm._ssrNode("<a href=\"/\" class=\"btn btn-primary orange\">","</a>",[_c('svg-icon',{attrs:{"name":"reachhemservice/mail"}}),_vm._ssrNode(" <span>Maila oss</span>")],2)],2)],2)])],2)])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/blocks/PagebuilderBasePriceCalculator.vue?vue&type=template&id=1711a9bc&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBasePriceCalculator.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderBasePriceCalculatorvue_type_script_lang_js_ = ({
  props: {
    data: {
      type: [Object, Array]
    }
  },

  data() {
    return {
      price: '0',
      area: '',
      message: ''
    };
  }

});
// CONCATENATED MODULE: ./components/blocks/PagebuilderBasePriceCalculator.vue?vue&type=script&lang=js&
 /* harmony default export */ var blocks_PagebuilderBasePriceCalculatorvue_type_script_lang_js_ = (PagebuilderBasePriceCalculatorvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/blocks/PagebuilderBasePriceCalculator.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(91)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  blocks_PagebuilderBasePriceCalculatorvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "095b3352"
  
)

/* harmony default export */ var PagebuilderBasePriceCalculator = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(92);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("9ede52d4", content, true, context)
};

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBasePriceCalculator_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(59);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBasePriceCalculator_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBasePriceCalculator_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBasePriceCalculator_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBasePriceCalculator_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 92:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".base_price-calculator-wrapper .container .row.gutter{margin:0 -2rem}.base_price-calculator-wrapper .container .row.gutter>*{padding:0 2rem}@media (max-width:767px){.base_price-calculator-wrapper .container .row.gutter{margin:0}.base_price-calculator-wrapper .container .row.gutter>*{padding:0}}.base_price-calculator-wrapper .container .row .form-inner{background:#fff;box-shadow:10px 10px 30px rgba(16,24,32,.1)}.base_price-calculator-wrapper .container .row .form-inner form{display:flex;justify-content:space-between;align-items:center;flex-wrap:wrap;width:100%}.base_price-calculator-wrapper .container .row .form-inner .input-item{width:48%}@media (max-width:1100px){.base_price-calculator-wrapper .container .row .form-inner .input-item{width:100%;margin-bottom:1rem}}.base_price-calculator-wrapper .container .row .form-inner .input-item label{width:100%;font-style:italic;font-family:\"DM Sans\",sans-serif}.base_price-calculator-wrapper .container .row .form-inner .payment{display:flex;justify-content:space-between;align-items:center;flex-wrap:wrap;width:100%;margin-top:2rem}.base_price-calculator-wrapper .container .row .form-inner .payment h4{margin:0}@media (max-width:1100px){.base_price-calculator-wrapper .container .row .form-inner .payment h4{width:100%}}.base_price-calculator-wrapper .container .row .form-inner .payment h4:nth-child(2){color:#3ea938;font-size:32px;font-weight:700}.base_price-calculator-wrapper .container .row .form-inner .payment h4 a{color:#101820;text-decoration:none;box-shadow:0 2px 0 0 #101820;transition:all .25s ease}.base_price-calculator-wrapper .container .row .form-inner .payment h4 a:hover{box-shadow:0 2px 0 0 #3ea938;color:#3ea938}@media (max-width:1100px){.base_price-calculator-wrapper .container .row .form-inner .payment span{width:100%}}.base_price-calculator-wrapper .container .row .form-inner .message{margin:2rem 0 1rem;display:block;padding:1rem;background:rgba(255,69,0,.1);border-radius:.8rem}.base_price-calculator-wrapper .container .row .form-inner .buttons-wrapper{margin-top:2rem}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=21.js.map