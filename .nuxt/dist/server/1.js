exports.ids = [1];
exports.modules = {

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseBlockTop.vue?vue&type=template&id=23c7dfa8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.title)?_c('div',{staticClass:"row"},[_vm._ssrNode("<div class=\"col-12 block-top\">","</div>",[_vm._ssrNode("<h2>"+_vm._ssrEscape("\n      "+_vm._s(_vm.title)+"\n    ")+"</h2> "),(_vm.url)?_vm._ssrNode("<div class=\"buttons-wrapper\">","</div>",[_c('nuxt-link',{staticClass:"btn btn-secondary",attrs:{"to":_vm.url}},[_c('span',[_vm._v(_vm._s(_vm.linkTitle))]),_vm._v(" "),_c('svg-icon',{attrs:{"name":"reachhemservice/chevron"}})],1)],1):_vm._e()],2)]):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/BaseBlockTop.vue?vue&type=template&id=23c7dfa8&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/BaseBlockTop.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseBlockTopvue_type_script_lang_js_ = ({
  props: {
    title: String,
    url: String,
    linkTitle: String
  }
});
// CONCATENATED MODULE: ./components/BaseBlockTop.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_BaseBlockTopvue_type_script_lang_js_ = (BaseBlockTopvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/BaseBlockTop.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_BaseBlockTopvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "395900ee"
  
)

/* harmony default export */ var BaseBlockTop = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=1.js.map