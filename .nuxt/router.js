import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _20cd873d = () => interopDefault(import('../pages/nyheter/index.vue' /* webpackChunkName: "pages/nyheter/index" */))
const _032b0d23 = () => interopDefault(import('../pages/tjanster/index.vue' /* webpackChunkName: "pages/tjanster/index" */))
const _f918dd2c = () => interopDefault(import('../pages/nyheter/_post.vue' /* webpackChunkName: "pages/nyheter/_post" */))
const _7cd097e4 = () => interopDefault(import('../pages/tjanster/_services.vue' /* webpackChunkName: "pages/tjanster/_services" */))
const _0bf357b3 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _5eaf28dc = () => interopDefault(import('../pages/_page/index.vue' /* webpackChunkName: "pages/_page/index" */))
const _034caaa3 = () => interopDefault(import('../pages/_page/_subPage/index.vue' /* webpackChunkName: "pages/_page/_subPage/index" */))
const _16dac13f = () => interopDefault(import('../pages/_page/_subPage/_subSubPage.vue' /* webpackChunkName: "pages/_page/_subPage/_subSubPage" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/nyheter",
    component: _20cd873d,
    name: "nyheter"
  }, {
    path: "/tjanster",
    component: _032b0d23,
    name: "tjanster"
  }, {
    path: "/nyheter/:post",
    component: _f918dd2c,
    name: "nyheter-post"
  }, {
    path: "/tjanster/:services",
    component: _7cd097e4,
    name: "tjanster-services"
  }, {
    path: "/",
    component: _0bf357b3,
    name: "index"
  }, {
    path: "/:page",
    component: _5eaf28dc,
    name: "page"
  }, {
    path: "/:page/:subPage",
    component: _034caaa3,
    name: "page-subPage"
  }, {
    path: "/:page/:subPage/:subSubPage",
    component: _16dac13f,
    name: "page-subPage-subSubPage"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
