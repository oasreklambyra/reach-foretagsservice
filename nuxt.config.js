const axios = require('axios')

module.exports = {
  mode: 'universal',
  telemetry: false,

  server: {
    host: '0.0.0.0'
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'Reach Fastighetsservice är en pålitlig partner inom lokalvård',
    htmlAttrs: {
      lang: 'sv-SE'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/static/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;700&family=Spectral:wght@200&display=swap' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/BaseLoadingPage.vue',
  /*
   ** Global CSS
   */
  css: ['@assets/bootstrap/bootstrap-config.scss', '@assets/scss/fonts.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-lazyload.js' },

    { src: '~/plugins/globalFunctions.js' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    [
      '@nuxtjs/gtm',
      {
        id: 'GTM-XXXX',
        autoInit: false,
        pageTracking: true
      }
    ]
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      '@nuxtjs/redirect-module',
      [
        { from: '^/wp-admin', to: 'https://wordpress.reachforetagsservice.se/wp-admin' }
      ]
    ],
    [
      'bootstrap-vue/nuxt',
      {
        bootstrapCSS: false,
        bootstrapVueCSS: false,
        components: ['BButton', 'BFormInput', 'BFormCheckboxGroup', 'BModal', 'BBadge', 'BTabs', 'BTab', 'BFormInvalidFeedback', 'BCarousel', 'BCarouselSlide', 'BBadge'],
        directives: ['BTooltip']
      }
    ],
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/svg-sprite',
    '@nuxtjs/style-resources',
    [
      '@nuxtjs/sitemap',
      {
        gzip: true,

        async routes () {
          const res = []

          const pages = await axios.get(
            'https://wordpress.reachforetagsservice.se/wp-json/core/sitemap'
          )

          return res
        }
      }
    ],
    [
      '@nuxtjs/robots',
      [
        {
          UserAgent: '*'
        }
      ]
    ]
  ],

  styleResources: {
    scss: ['./assets/scss/variables.scss']
  },

  router: {
    middleware: ['getMeta', 'getPage', 'getSupportData']
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    transpile: ['gsap'],

    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          options: 'fix',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))

      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        use: ['babel-loader', 'vue-svg-loader']
      })
    }

    /* extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    }
    */
  }
}
