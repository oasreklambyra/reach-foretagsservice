export const state = () => ({
  pages: {},
  cpt: {},
  tax: {},
  cf: { },
  options: false,
  pageLocked: false,
  password: null,
  sitemap: false,
  wordpress: 'https://wordpress.reachforetagsservice.se/wp-json/'

})

export const mutations = {

  getPage (state, data) {
    const temporaryHolder = state.pages
    state.pages = [] // Updates the structure
    temporaryHolder[data.path] = data.data
    state.pages = temporaryHolder
  },
  getSitemap (state, data) {
    state.sitemap = data
  },
  getOptions (state, data) {
    state.options = data.data
  },
  getCpt (state, data) {
    const temporaryHolder = state.cpt
    state.cpt = [] // Updates the structure
    temporaryHolder[data.path] = data.data
    state.cpt = temporaryHolder
  },
  getTax (state, data) {
    const temporaryHolder = state.tax
    state.tax = [] // Updates the structure
    temporaryHolder[data.type] = data.data
    state.tax = temporaryHolder
  },

  isPageLocked (state, data) {
    state.pageLocked = data.data
  },

  setPassword (state, data) {
    state.password = data.data
  }
}

export const getters = { // Good to use if multi lang page
  getPage (state) {
    return state.pages
  },
  getSitemap (state) {
    return state.sitemap
  },
  getOptions (state) {
    return state.options
  },
  getCpt (state) {
    return state.cpt
  },
  getTax (state) {
    return state.tax
  }
}

export const actions = {

  nuxtServerInit ({ dispatch }, { app }) {
    dispatch('session/init')
  },

  async getPage (context, payload) {
    const type = (payload.postType) ? payload.postType : 'any'

    const res = await this.$axios.get(context.state.wordpress + 'core/page?slug=' + payload.path + '&type=' + type)

    if (res.data && Array.isArray(res.data)) {
      res.data.forEach((data) => {
        if (data && data.custom_fields && data.custom_fields.is_page_locked && data.custom_fields.password) {
          context.commit('setPassword', {
            data: data.custom_fields.password
          })

          context.commit('isPageLocked', {
            data: true
          })
        } else {
          context.commit('setPassword', {
            data: null
          })

          context.commit('isPageLocked', {
            data: false
          })
        }

        /* OLD WAY */
        context.commit('getPage', {
          path: payload.postType + '/' + payload.path,
          data
        })

        /* NEW WAY - Used in posttype pages */
        context.commit('getPage', {
          path: data.permalink,
          data
        })
      })
    }
  },

  async getCpt (context, payload) {
    const res = await this.$axios.get(context.state.wordpress + 'core/cpt?type=' + payload.postType)

    let data = false

    if (res.data[0]) {
      data = res.data
    }

    context.commit('getCpt', {
      path: payload.postType,
      data
    })
  },
  async getTax (context, payload) {
    const res = await this.$axios.get(context.state.wordpress + 'core/taxonomy?type=' + payload.taxType)

    context.commit('getTax', {
      type: payload.taxType,
      data: res.data
    })
  },
  async getSitemap (context, payload) {
    const res = await this.$axios.get(context.state.wordpress + 'core/sitemap')
    context.commit('getSitemap', res.data)
  },

  async getOptions (context, payload) {
    const res = await this.$axios.get(context.state.wordpress + 'core/options')
    context.commit('getOptions', {
      data: res.data
    })
  },

  checkPassword (context, payload) {
    context.commit('isPageLocked', {
      data: !(payload.pwd === context.state.password)
    })

    return payload.pwd === context.state.password
  }
}
