export const state = () => ({
  cookie: false
})

export const mutations = {
  setCookie (state, data) {
    state.cookie = data
  }
}

export const getters = { // Good to use if multi lang page

}

export const actions = {
  init (context, payload) {
    const cookies = this.$cookies.getAll()
    const keys = Object.keys(cookies)

    let activeUserCookie = false

    keys.forEach((key) => {
      if (key.startsWith('wordpress_logged_in_')) {
        activeUserCookie = cookies[key]
      }
    })

    context.commit('setCookie', activeUserCookie)
  }
}
