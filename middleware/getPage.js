/*
  This file fetches the page from the headless CMS.
 */

export default async function (context) {
  const { route } = context

  const data = context.app.router.app.getPageLogic(route)

  if (!context.app.store.state.pages[data.type + '/' + data.name]) {
    await context.app.store.dispatch({ type: 'getPage', path: data.name, postType: data.type })
  }
}
